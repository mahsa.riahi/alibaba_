import React from 'react';
import {NavLink , Link} from "react-router-dom"

const TopNav = (props) => {
    return ( 

        <nav id="nav">
        <Link id="nav-img-container"> 
            <img id="nav-img"  src="https://cdn.alibaba.ir/dist/4013b46e/img/logo.f05d292.svg"/>
            <img id="nav-img" src="https://cdn.alibaba.ir/dist/4013b46e/img/logo-type.71f598a.svg"/>
         
        </Link>

         <ul className="row" id="right-nav">
             <li id="right-nav-item" className="col-lg-2 hidden-m hidden-s hidden-xs flight-item">هواپیما
                 <ul id="flight-items">
                    <Link id="flight-item" to="/">پرواز داخلی</Link>
                    <Link id="flight-item" to="/outside-flight">پرواز داخلی</Link>
                </ul>
             </li>
             <NavLink id="right-nav-item" className="col-lg-2 hidden-m hidden-s hidden-xs" to="/train">قطار</NavLink>
             <NavLink id="right-nav-item" className="col-lg-2 hidden-m hidden-s hidden-xs" to="/">اتوبوس</NavLink>
             <NavLink id="right-nav-item" className="col-lg-2 hidden-m hidden-s hidden-xs" to="/">تور</NavLink>
             <NavLink id="right-nav-item" className="col-lg-2 hidden-m hidden-s hidden-xs" to="/">هتل</NavLink>
             <NavLink id="right-nav-item" className="col-lg-2 hidden-m hidden-s hidden-xs" to="/">ویلا و اقامتگاه</NavLink>
    
          </ul>

          <ul className="row" id="left-nav">
              <NavLink id="left-nav-item" className="col-lg-5 col-md-5 col-s-5 col-xs-5" to="/register">ثبت نام</NavLink> 
              <span id="left-nav-item" className="col-lg-1 col-md-1 col-s-1 col-xs-1">|</span>
              <NavLink id="left-nav-item" className="col-lg-3 col-md-3 col-s-3 col-xs-3" to="/login">ورود</NavLink>
             
             
          </ul>
    
    </nav>
     );
}
 
export default TopNav;