import React from "react"
import {Link} from "react-router-dom"
import Form from "./form";

const MainNav = (props) => {
    return ( 

        <nav id="second-nav">
               <div id="gray"></div>
               <div id="white"></div>

                 <ul className="row">
                  
                 <li>
                     <Link id="nav-item" to="/">
                     <span>
                     <img className="nav-png" src="https://s4.uupload.ir/files/air-mail_6m9.png" />
                     </span>
                     <h3 className="nav-service">پرواز داخلی </h3>
                     </Link>
                    </li>  
                 <li>
                     <Link  id="nav-item" to="/outside-flight">
                     <span>
                     <img className="nav-png" src="https://s4.uupload.ir/files/transportation_ops5.png" />
                     </span>
                     <h3 className="nav-service">پرواز داخلی </h3>
                     </Link>
                 </li>
                 <li>
                     <Link id="nav-item" to="/train">
                     <span>
                     <img className="nav-png" src="https://s4.uupload.ir/files/train_p4fu.png" /></span>
                     <h3 className="nav-service">قطار </h3>
                     </Link>
                 </li>
                 <li>
                     <Link  id="nav-item" to="/">
                     <span>
                     <img className="nav-png" src="https://s4.uupload.ir/files/caravan_t8nh.png" />
                     </span>
                     <h3 className="nav-service">اتوبوس </h3>
                      </Link>
                 </li>  
                 <li>
                     <Link  id="nav-item" to="/">
                     <span>
                     <img className="nav-png" src="https://s4.uupload.ir/files/backpack_7ix0.png" />
                     </span>
                    <h3 className="nav-service">تور</h3>
                     </Link>
                </li>
                <li>  
                     <Link  id="nav-item" to="/">
                     <span>
                     <img className="nav-png" src="https://s4.uupload.ir/files/single-bed_eqh.png" />
                     </span>
                     <h3 className="nav-service"> هتل خارجی و داخلی </h3>
                     </Link>
                 </li>    
                
                 </ul>

                 <Form />
       
              
     
        </nav>


    
     );
}
 
export default MainNav ;