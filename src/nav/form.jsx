import Passenger from "../utils/passenger"
import React ,{useState } from "react"
import Cities1 from "../utils/city"
import Cities2 from "../utils/city2"
import { useSelector } from "react-redux"
import CustomDatePicker from "../utils/calender"
import { withRouter } from "react-router";


const Form = ({location}) =>{

      const [getShow1 , setShow1] = useState(false)
      const [getShow2 , setShow2] = useState(false)
      const [getShow3 , setShow3 ]= useState(false)
      const [getShow4 , setShow4] = useState(false)
      const [getShow5 , setShow5 ]= useState(false)
      const [getCity1 , setCity1] = useState("")
      const [getCity2 , setCity2] = useState("")
      const [getContent1 , setContent1] = useState()
      const [getContent2 , setContent2] = useState()
      const [getContent3 , setContent3] = useState()
      const [getContent4 , setContent4] = useState()
      const [getContent5 , setContent5] = useState()
      const [checked , setChecked] = useState("oneway")
     

      const numbers = useSelector(state => state.passNum)
      const cityName = useSelector(state => state.handleCitiesName)
      const cityName1 = useSelector(state => state.handleCitiesName1)
      console.log(numbers)


 const handleClick1=() =>{
    setContent2(null)
    setContent3(null)
     setShow1(!getShow1)
     if(getShow1){
        setContent1( <Cities1 />)
        }
        else{
            setContent1(null)

        }   
     }
     const handleClick2 =() =>{

        setContent1(null)
        setContent3(null)
         
        setShow2(!getShow2)
        if(getShow2){
           setContent2( <Cities2 />)
           }
           else{
               setContent2(null)
           }
     }
     const handleChange1=(event) =>{
         setCity1(event.target.value)

     }
     const handleChange2=(event) =>{
        setCity2(event.target.value)

    }

    const handleClick3=() =>{
        setContent2(null)
        setContent1(null)
        setShow3(!getShow3)

        if(getShow3){
            setContent3(<Passenger />)
        }
        else{
            setContent3(null)
        }

    }
    const handleClick4=() =>{
        setContent2(null)
        setContent1(null)
        setContent3(null)
        setContent5(null)


    }
    const handleClick5=() =>{
        setContent2(null)
        setContent1(null)
        setContent3(null)
        setContent4(null)
     

    }
    const handleChecked =(event) =>{ 
        setChecked(event.currentTarget.value)
       
    }

    
 
    return(

        <div>
        <form id="nav-form">
         <div id="form-checkbox">
           
         <div>
              
              <input  type="radio" value="oneway" checked={checked === "oneway"} onChange={handleChecked} ></input>
              <div>یک طرفه</div>
              </div>
              <div>
              <input type="radio"  value="return" checked={checked === "return"} onChange={handleChecked}></input>
              <div>رفت و برگشت</div>
              </div>
              <div>
              {location.pathname === "/outside-flight" ?
              <input type="radio" value="multiway" checked={checked === "multiway"} onChange={handleChecked}></input> : null}
              {location.pathname === "/outside-flight" ? <div>چند مسیره</div>
              : null}
              </div>
            

          </div>
        <ul>
        <li>
        <input name="source" id="form-item1" placeholder="مبدا" type="text" onClick={handleClick1} onChange={handleChange1} value={cityName}></input>
        <label >{getContent1}</label>
        <input name="destination" id="form-item1" placeholder="مقصد" type="text" onClick={handleClick2} onChange={handleChange2} value={cityName1}></input>
        <label>{getContent2}</label>
        </li>
        <li>
        <div className="form-item2"> <CustomDatePicker onClick={handleClick4} /></div>
        <div className="form-item3"> <CustomDatePicker onClick={handleClick5} /></div>

        </li>
       <li>
       <div id="form-item4" onClick={handleClick3}>{`${numbers} مسافر`}</div>
       <div>{getContent3}</div>
       </li> 
        <li><span id="search-button" type="submit">جستجو</span></li>
        </ul>
        
        </form>
        
        </div>



    )

}

export default withRouter(Form);