import React , {useEffect} from "react"
import MainLayout from './layout/MainLayout';
import {Switch , Route} from "react-router-dom"
import InsideFlight from "./component/insideflight";
import OutsideFlight from "./component/outsideflight";
import Train from './component/train';





const App = (props) => {


  useEffect(() =>{
    require("./utils/script")


},)


  return ( 
  
        <MainLayout>

        <Switch>
        <Route path="/" exact component={InsideFlight} />
        <Route path="/outside-flight" component={OutsideFlight} />
        <Route path="/train" component={Train} />
        
        
        
        </Switch>
           

        </MainLayout>

   );
}

export default App;