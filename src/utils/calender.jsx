import React, { useState } from "react"
import DatePicker from "react-multi-date-picker"
import persian from "react-date-object/calendars/persian"
import persian_fa from "react-date-object/locales/persian_fa"
import "react-multi-date-picker/styles/layouts/mobile.css"


const CustomDatePicker=()=> {
  const [value, setValue] = useState("تاریخ")
  const weekDays = ["ش", "ی", "د", "س", "چ", "پ", "ج"]

  return (
    <DatePicker 
      value={value}
      onChange={setValue}
      calendar={persian}
      locale={persian_fa}
      calendarPosition="bottom-right"
      weekDays={weekDays}
      inputClass="custom-input"
      className="custom-calendar"

    />
    
    
  )
}


export default CustomDatePicker