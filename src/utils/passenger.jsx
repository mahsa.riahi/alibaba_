import { useState } from "react"
import { useDispatch} from "react-redux"
import { handleIncrease  , handleDecrease} from './../actions/handlenumber';


export const Passenger =() =>{

    const [getNum1 , setNum1] = useState(1)
    const [getNum2 , setNum2] = useState(0)
    const [getNum3 , setNum3] = useState(0)
    const [PassengerNum , setpassengerNum] = useState(1)



 
     const dispatch = useDispatch()


    const handleClick1 =() =>{
        setNum1(getNum1+1)
        setpassengerNum(PassengerNum+1)
        dispatch(handleIncrease())
      

    }
    const handleClick2 =() =>{
        setNum2(getNum2+1)
        setpassengerNum(PassengerNum+1)
        dispatch(handleIncrease())
    }
    const handleClick3 =() =>{
        setNum3(getNum3+1)
        setpassengerNum(PassengerNum+1)
        dispatch(handleIncrease())
    }
    const handleClick4 =() =>{
        if(getNum1===1){
            setNum1(1)
            
        }
        else{
        setNum1(getNum1-1)
        setpassengerNum(PassengerNum-1)
        dispatch(handleDecrease())
        }
      
    }
    const handleClick5 =() =>{
        if(getNum2===0){
            setNum2(0)
        
        }
        else{
        setNum2(getNum2-1)
        setpassengerNum(PassengerNum-1)
        dispatch(handleDecrease())
        }
  
    }
    const handleClick6 =() =>{
        if(getNum3===0){
            setNum3(0)
           
        }
        else{
        setNum3(getNum3-1)
        setpassengerNum(PassengerNum-1)
        dispatch(handleDecrease())
        }

    }
 

    return(
       
        
    <div>
       <ul id="passenger">
       <li className="passenger-item">بزرگسال (12 به بالا)<div><img className="plus" src="https://s4.uupload.ir/files/add(1)_5sni.png" onClick={handleClick1}></img>{getNum1}<img className="minus" src="https://s4.uupload.ir/files/minus_1yl1.png" onClick={handleClick4}></img></div></li>
       <li className="passenger-item">کودک (2 سال تا 12 سال)<div><img className="plus" src="https://s4.uupload.ir/files/add(1)_5sni.png" onClick={handleClick2}></img>{getNum2}<img className="minus" src="https://s4.uupload.ir/files/minus_1yl1.png" onClick={handleClick5}></img></div></li>
       <li className="passenger-item">نوزاد (10 روز تا 2 سال)<div><img className="plus" src="https://s4.uupload.ir/files/add(1)_5sni.png" onClick={handleClick3}></img>{getNum3}<img className="minus" src="https://s4.uupload.ir/files/minus_1yl1.png" onClick={handleClick6}></img></div></li>
       
       </ul>
    
    </div>

    
    )
}

export default Passenger