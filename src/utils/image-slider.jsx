

    import React from 'react';
    import Slider from 'infinite-react-carousel';
    
    const SimpleSlider = () => (
      <Slider dots>
        <div>
        <img src="https://s4.uupload.ir/files/capture_x9fc.jpg" />
        </div>
        <div>
          <h3>2</h3>
        </div>
        <div>
          <h3>3</h3>
        </div>
        <div>
          <h3>4</h3>
        </div>
        <div>
          <h3>5</h3>
        </div>
      </Slider>
    );
    
export default SimpleSlider ;
