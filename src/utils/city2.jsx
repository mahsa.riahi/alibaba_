import { useDispatch } from "react-redux"
import { cityName1 } from './../actions/cityname1';

const options= [
    { value: 'تهران', id: 1 },
    { value: 'اهواز', id: 2 },
    { value: 'شیراز', id: 3 },
    { value: 'مشهد', id: 1 },
    { value: 'بندرعباس', id: 2 },
    { value: 'اصفهان', id: 3 },
    { value: 'تبریز', id: 1 },
    { value: 'کیش', id: 2 },
    { value: 'آبادان', id: 3 },
    { value: 'اراک', id: 1 },
    { value: 'اردبیل', id: 2 },
    { value: 'ارومیه', id: 3 },
    { value: 'ایرانشهر', id: 1 },
    { value: 'ایلام', id: 2 },
    { value: 'بجنورد', id: 3 }
  ]
  

const Cities2 =() =>{

    const dispatch= useDispatch()

    return(
        <div>

    <ul className="citieslist1">{ options.map(option => <li  className="citiesname1" key={option.id} onClick={() => dispatch(cityName1(option.value))}>{option.value}</li>)}</ul>
    </div>
  
    )
}

export default Cities2