import React from "react"
import MainNav from './../nav/MainNav';
import TopNav from './../nav/topNav';
import Header from './../component/Header';
import Footer1 from "./../component/footer1";
import Footer2 from "./../component/footer2";



const MainLayout = (props) => {
    return ( 
       <div>
           <TopNav />
           <MainNav />
           <Header />
       
        
           {props.children}

           <Footer1 />
           <Footer2 />

           

        </div>
     );
}
 
export default MainLayout;