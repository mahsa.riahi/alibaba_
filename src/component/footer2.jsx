import {Link} from "react-router-dom"

const Footer2 = () => {
    return (  
        <footer id="footer2">
        <div >
            <div id="footer-div1-2" className="footer-div1-2">
                <h1>علی‌بابا</h1>
                <ul>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>درباره ما</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>تماس با ما </Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>چرا علی‌بابا</Link> </li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>باشگاه مشتریان </Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>فروش سازمانی </Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>قوانین و مقررات</Link> </li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>راهنمای خرید </Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>پرسش و پاسخ</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>مجله علی‌بابا</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>فرصت‌های شغلی</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>همکاری با آژانس‌ها</Link></li>
                </ul>
            </div>

            <div  id="Footer-div1-2" className="footer-div1-2">
            <h1>اطلاعات تکمیلی</h1>
            <ul>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>بلیط چارتر</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور کیش</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور استانبول</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>راهنمای خرید بلیط قطار </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>راهنمای خرید بلیط اتوبوس </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>راهنمای رزرو هتل خارجی </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>راهنمای استرداد بلیط </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>بلیط قطار </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>خرید بلیط هواپیما خارجی </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>اطلاعات فرودگا‌هی </Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>شیوه‌نامه حقوق مسافر</Link></li>
            </ul>
        </div>

        <div id="footer-div2-2" className="footer-div2-2">
            <h1>اپلیکیشن علی‌بابا </h1>
            <p>با نصب اپلیکیشن علی‌بابا بلیط همه سفرها در جیب شماست. آسان‌ترین، کامل‌ترین و مطمئن‌ترین روش تهیه بلیط و هتل را با اپلیکیشن علی‌بابا تجربه کنید. </p>
            <div>
                <img id="img1-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/sibapp-2x.952a44c.png?w=125&h=42&q=100" />
                <img id="img1-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/sibirani-2x.4bba476.png" />
                <img id="img1-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/cafebazaar-2x.296519f.png?w=125&h=42&q=100" />
                <img id="img1-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/cafebazaar-2x.296519f.png?w=125&h=42&q=100" />
                <img className="dl-app-mobile" id="img1-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/pwa-footer.e23d317.svg?w=125&h=42&q=100" />
            </div>
        </div>

        <div id="Footer-div2-2" className="footer-div2-2">
            <img className="alibaba-cdn" src="https://cdn.alibaba.ir/dist/598409ea/img/footer-logo.ef8be5e.svg" />
            <br />
            <div>
            <img id="img2-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/kasb.f1720ad.png" />
            <img id="img2-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/passenger-rights.9eea7f0.svg" />
            <img id="img2-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/aira.0d558e5.png" />
            <img id="img2-footer" src="https://cdn.alibaba.ir/dist/598409ea/img/state-airline.317506d.svg" />
            </div>
        </div>
        </div>
        </footer>
    );
}
 
export default Footer2;