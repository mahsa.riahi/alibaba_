import React , {Fragment} from 'react';
import AppDl from './app-dl-section';
import OfQuesSec from './flightComponents/OF-common-question';
import OfIntro from './flightComponents/OF-intro';


const OutsideFlight= () => {
    return ( 

        <Fragment>
             <div id="nav-plane-img"  className="nav-img"></div>
             <AppDl />
             <OfIntro />
             <OfQuesSec /> 
        
        
        </Fragment>

     );
}
 
export default OutsideFlight;