import SimpleImageSlider from "react-simple-image-slider";

const images = [
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
  { url: "https://cdn.alibaba.ir/dist/66efd5c0/img/jabama-domestic.5738729.jpg?w=580&h=220&q=100" },
];

const Slider = () => {
  return (
    <div>
      <SimpleImageSlider
        width={896}
        height={504}
        images={images}
      />
    </div>
  );
}

export default Slider;