import React , {Fragment , useEffect} from 'react';
import AppDl from './app-dl-section';
import Menu from './../component/flightComponents/menu';
import IfIntro from './../component/flightComponents/IF-intro';
import IfQuesSec from './flightComponents/IF-common-question';



const InsideFlight= () => {

 
    return ( 

        <Fragment>
             <div id="nav-plane-img"  className="nav-img"></div>
             <AppDl />
             <Menu />
             <IfIntro />
             <IfQuesSec />

         
        </Fragment>

     );
}
 
export default InsideFlight;