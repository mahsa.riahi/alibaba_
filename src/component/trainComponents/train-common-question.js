import { useCallback, useRef } from 'react';
import $ from "jquery"


const TrainQuesSec= () => {


    const slideToggle = useRef(null)
    const slideToggle1 = useRef(null)
    const slideToggle2 = useRef(null)
    const slideToggle3 = useRef(null)
    const slideToggle4 = useRef(null)
    const slideToggle5 = useRef(null)
    const slideToggle6 = useRef(null)
    const slideToggle7 = useRef(null)
    const slideToggle8 = useRef(null)
    const slideToggle9 = useRef(null)


    const handleSlideToggle = useCallback(() =>{
        $(slideToggle.current).slideToggle("400");
    
    },[])
    const handleSlideToggle1 = useCallback(() =>{
        $(slideToggle1.current).slideToggle("400");
    
    },[])
    const handleSlideToggle2 = useCallback(() =>{
        $(slideToggle2.current).slideToggle("400");
      
    },[])
    const handleSlideToggle3 = useCallback(() =>{
        $(slideToggle3.current).slideToggle("400");
     
    },[])
    const handleSlideToggle4 = useCallback(() =>{
        $(slideToggle4.current).slideToggle("400");
       
    },[])
    const handleSlideToggle5 = useCallback(() =>{
        $(slideToggle5.current).slideToggle("400");

    },[])
    const handleSlideToggle6 = useCallback(() =>{
        $(slideToggle6.current).slideToggle("400")
    },[])
    const handleSlideToggle7 = useCallback(() =>{
        $(slideToggle7.current).slideToggle("400")
    },[])
    const handleSlideToggle8 = useCallback(() =>{
        $(slideToggle8.current).slideToggle("400")
    },[])
    const handleSlideToggle9 = useCallback(() =>{
        $(slideToggle9.current).slideToggle("400")
    },[])


    return ( 

        <div id="ques-container">
        <h1 id="train-ques-title">پرسش های شما</h1>
        <ul>
             <li onClick={handleSlideToggle}>
             <div id="train-ques1"  ><i className="bi bi-question-circle"></i>
              <p>
             در راه آهن و هنگام سوار شدن به قطار چه مدارکی لازم است؟
             </p>
             </div>
             <div ref={slideToggle}  >شما برای ورود به قطار باید این مدارک را با خود به همراه داشته باشید: پرینت بلیط و کارت شناسایی معتبر.<br/>در صورت داشتن تخفیف جانباز یا خانواده شهدا، به همراه داشتن کارت معتبر مربوطه نیاز ضروری است.</div>
            </li>
            <li onClick={handleSlideToggle1}>
            <div  id="train-ques2" > <i className="bi bi-question-circle"></i><p> 
            مقدار بار مجاز در قطار چقدر است؟
            </p></div>
            <div  ref={slideToggle1} >هر مسافر می‌تواند وسایل ضروری سفر خود را همراه ببرد به شرط آن‌كه حجم آن از ابعاد 75 سانتی متر و وزن 30 كیلوگرم تجاوز نكند. به عنوان مثال مسافر می‌تواند كیف‌دستی، بسته‌ و چمدان‌های كوچك محتوی لوازم شخصی، صندلی چرخ‌دار بیمار، كالسكه بچه و قفس كوچك پرندگان ریزجثه (حداكثر در ابعاد 40 سانتی‌متر) را به همراه داشته باشد.</div>
           </li>
           <li onClick={handleSlideToggle2}>
           <div  id="train-ques3" ><i className="bi bi-question-circle"></i><p> 
           روال استرداد یا کنسلی بلیط قطار چگونه است؟
           </p> </div>
           <div  ref={slideToggle2} >مسافر می‌تواند تا 60 دقیقه قبل از حرکت قطار، بلیط یا بلیط‌های خریداری‌شده را به صورت آنلاین استرداد کند.<br/>برای استرداد بلیط قطار پس از زمان ذکرشده، مسافر باید تا قبل از حرکت قطار، و با ارایه بلیط چاپی و کارت شناسایی به صورت حضوری به یکی از مراکز فروش بلیط قطار مراجعه کند.</div>
          </li>
          <li onClick={handleSlideToggle3}>
          <div  id="train-ques4" ><i className="bi bi-question-circle"></i><p>
          قیمت بلیط برای کودک و نوزاد چگونه است؟
          </p></div>
          <div  ref={slideToggle3} >قیمت بلیط کودک نیم‌بها و نوزاد 10 درصد مبلغ بزرگسال است.</div>
         </li>
         <li onClick={handleSlideToggle4}>
         <div  id="train-ques5" ><i className="bi bi-question-circle"></i><p> 
         چه زمانی باید در ایستگاه حضور داشته باشم؟
         </p></div>
         <div  ref={slideToggle4} >حضور در ایستگاه یک ساعت قبل از حرکت قطار الزامی است. همچنین ده دقیقه مانده به حركت، تمام درهای قطار بسته خواهد شد و بعد از بسته‌شدن درها برای جلوگیری از خطرات احتمالی، سوارشدن به قطار امكان‌پذیر نیست.</div>
        </li>
        <li onClick={handleSlideToggle5}>
        <div  id="train-ques6" ><i className="bi bi-question-circle"></i><p>
        در یک کوپه دربست چند نفر همراه اضافه می‌توان برد؟
         </p></div>
        <div  ref={slideToggle5} >مسافر می‌تواند بعد از دربست‌کردن کوپه فقط یک نفر مسافر اضافی را بدون بلیط با این شرایط به همراه خود به قطار ببرد:<br/>
        <ul style={{marginRight : "30px"}}>
        <li>حق درخواست جا خارج از همان کوپه وجود ندارد؛</li>
        <li>قبض این مسافر مطابق استحقاق او توسط رئیس قطار صادر می‌شود.</li>
        </ul></div>
       </li>
       <li onClick={handleSlideToggle6}>
       <div  id="train-ques7" ><i className="bi bi-question-circle"></i><p>
       در صورت مفقودی بلیط قطار، چه باید کرد؟
        </p></div>
       <div  ref={slideToggle6} >در صورت مفقودی بلیط، مسافر می‌تواند در زمان اداری به ایستگاه‌های راه‌آهن که سیستم آنلاین فروش بلیط دارند، مراجعه کند و دوباره بلیط را دریافت کند. همچنین به شرط حضور به‌موقع (حداكثر ۴۵ دقیقه مانده به حركت) در ایستگاه‌های راه‌آهن كه سیستم فروش بلیط آنلاین دارند و در وقت اداری، بلیط المثنی طبق آیین‌نامه‌ مربوط صادر می‌شود.<br/>بلیط مفقودی باید از طریق شبكه‌ رایانه‌ای صادر شده و استرداد نشده باشد. صدور بلیط المثنی با ۵ درصد قیمت كل بلیط، انجام می‌شود و در صورت مفقود شدن آن، بلیط‌ المثنی دیگری صادر نخواهد شد. درحال حاضر بلیط المثنی از طریق سیستم استرداد نمی‌شود. (استرداد بلیط المثنی فقط به شرط ارائه‌ بلیط اصلی و بلیط المثنی به صورت همزمان در ایستگاه‌های آنلاین و كلیه‌ نمایندگی‌های فروش و با هماهنگی مركز یكپارچه فروش بلیط قطار انجام می‌شود).</div>
      </li>
      <li onClick={handleSlideToggle7}>
       <div  id="train-ques8" ><i className="bi bi-question-circle"></i><p> 
       آیا این امکان وجود دارد که مسافر در پکیج قطار با هتل فدک درخواست اقامت بیشتر در هتل داشته باشد؟ در صورت امکان، وضعیت آن به چه صورتی است؟
       </p></div>
       <div  ref={slideToggle7} >امکان اقامت بیشتر وجود دارد اما باید قبل از خرید با هتل مربوطه هماهنگ کرده باشند؛ همچنین تاریخ رفت و برگشت بلیط قطار باید بر اساس روزهایی باشد که در هتل می‌مانید. گفتنی است اقامت قطار فدک شامل شب اول و شب آخر می‌شود.</div>
      </li>
      <li onClick={handleSlideToggle8}>
       <div  id="train-ques9" ><i className="bi bi-question-circle"></i><p>
       آیا خانوم‌ها می‌توانند بلیط قطار را برای کوپه مخصوص بانوان بخرند؟
       </p></div>
       <div  ref={slideToggle8} >بله این امکان وجود دارد. خانوم‌ها می‌توانند برای آرامش و راحتی خود در طول سفر، بلیط قطار را برای کوپه مخصوص بانوان بخرند. البته کوپه مخصوص آقایان هم برای آن‌ها درنظر گرفته شده است.</div>
      </li>
      <li onClick={handleSlideToggle9}>
      <div  id="train-ques10" ><i className="bi bi-question-circle"></i><p> 
      شرایط بیمه قطار چه گونه است؟
      </p></div>
      <div  ref={slideToggle9} >تمام مسافران قطار از لحظه‌ ورود به ایستگاه مبدا تا خروج از ایستگاه مقصد در برابر حوادث بیمه هستند. بیمه‌ حوادث مسافری شامل جبران هزینه‌های پزشكی و همچنین جبران نقص عضو یا فوت ناشی از حوادثی مانندآتش سوزی، تصادف، انفجار، خارج شدن قطار از ریل، مانور، سنگ‌پرانی و سایر حوادث احتمالی می‌شود.</div>
     </li>

            
        
        </ul>
       
    </div>

     );
}
 
export default TrainQuesSec;