import {Link} from "react-router-dom"
import { useRef , useCallback, useState } from 'react';
import $ from 'jquery'




const Menu = () => {



    const slideToggle1 = useRef(null);
 
    

    const handleSlidedown = useCallback( ()=>{
        $(slideToggle1.current).slideToggle("10")
       
      

},[])


    return (

        <div id="container-info" >
            <div className="row" id="info-title" onClick={handleSlidedown}>
                 <h1 className="col-lg-3 col-md-3"> خواندنی ها </h1>
                 <h1 className="col-lg-3 col-md-3"> تور ها </h1>
                 <h1 className="col-lg-3 col-md-3"> همکاران ما </h1>
                 <h1 className="col-lg-3 col-md-3"> مسیر های پر تردد </h1>

             </div>
       
             <div className="row" id="detail-title" ref={slideToggle1}>
                <ul className="col-lg-3 col-md-3 col-s-3"  >
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>قیمت بلیط هواپیما</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>بلیط ارزان هواپیما</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط چارتر</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>بلیط لحظه آخری</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none" }}>تور دبی</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none" }}>تور استانبول</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none" }}>تور تایلند</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none" }}>تور سریلانکا</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none"}}>تور فیلیپین</Link></li>
                    <li><Link className="li" to="" style={{textDecoration : "none" }}>تور هنگ کنگ</Link></li>
                </ul>

            <ul className="col-lg-3 col-md-3 col-s-3" >
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور روسیه</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور مسکو</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none" }}>تور مالدیو</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور سیشل</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none" }}>تور اروپا</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور ایتالیا</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none" }}>تور اسپانیا</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none"}}>تور فرانسه</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none" }}>تور یونان</Link></li>
                <li><Link className="li" to="" style={{textDecoration : "none" }}>تور کرواسی</Link></li>
            </ul>

        <ul className="col-lg-3 col-md-3 col-s-3" >
            <li><Link className="li" to="" style={{textDecoration : "none" }}>ایران ایر </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>هواپیمایی ماهان </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>هواپیمایی کاسپین </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>قطار رجا </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>قطار فدک </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>اطلس جت </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>ترکیش ایر </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>هواپیمایی قطر </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>لوفتهانزا </Link></li>
            <li><Link className="li" to="" style={{textDecoration : "none" }}>ایرفرانس </Link></li>
        </ul>

    <ul className="col-lg-3 col-md-3 col-s-3"  >
        <li><Link className="li" to=" " style={{textDecoration : "none" }}>پرواز تهران مشهد</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>پرواز مشهد تهران</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار تهران مشهد</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار مشهد تهران</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار تهران کرمان</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار کرمان تهران</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار مشهد قم</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار قم مشهد</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار تهران بندرعباس</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار بندرعباس تهران</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار تهران یزد</Link></li>
        <li><Link className="li" to="" style={{textDecoration : "none" }}>بلیط قطار یزد تهران</Link></li>
    </ul>

             </div>

             

        
        </div>
      );
}
 
export default Menu;