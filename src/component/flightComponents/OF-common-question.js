import { useCallback, useRef } from 'react';
import $ from "jquery"


const OfQuesSec= () => {


    const slideToggle = useRef(null)
    const slideToggle1 = useRef(null)
    const slideToggle2 = useRef(null)
    const slideToggle3 = useRef(null)
    const slideToggle4 = useRef(null)
    const slideToggle5 = useRef(null)
    const slideToggle6 = useRef(null)
    const slideToggle7 = useRef(null)
    const slideToggle8 = useRef(null)
    const slideToggle9 = useRef(null)


    const handleSlideToggle = useCallback(() =>{
        $(slideToggle.current).slideToggle("400");
    
    },[])
    const handleSlideToggle1 = useCallback(() =>{
        $(slideToggle1.current).slideToggle("400");
    
    },[])
    const handleSlideToggle2 = useCallback(() =>{
        $(slideToggle2.current).slideToggle("400");
      
    },[])
    const handleSlideToggle3 = useCallback(() =>{
        $(slideToggle3.current).slideToggle("400");
     
    },[])
    const handleSlideToggle4 = useCallback(() =>{
        $(slideToggle4.current).slideToggle("400");
       
    },[])
    const handleSlideToggle5 = useCallback(() =>{
        $(slideToggle5.current).slideToggle("400");

    },[])
    const handleSlideToggle6 = useCallback(() =>{
        $(slideToggle6.current).slideToggle("400")
    },[])
    const handleSlideToggle7 = useCallback(() =>{
        $(slideToggle7.current).slideToggle("400")
    },[])
    const handleSlideToggle8 = useCallback(() =>{
        $(slideToggle8.current).slideToggle("400")
    },[])
    const handleSlideToggle9 = useCallback(() =>{
        $(slideToggle9.current).slideToggle("400")
    },[])


    return ( 

        <div id="ques-container">
        <h1 id="ques-title">پرسش های شما</h1>
        <ul>
             <li onClick={handleSlideToggle}>
             <div id="of-ques1" ><i className="bi bi-question-circle"></i> <p>بلیط پرواز چه کشورها و ایرلاین‌هایی را می‌توانم‌ در سایت علی‌بابا جستجو و خریداری کنم؟</p></div>
             <div ref={slideToggle} >بلیط تمام خطوط هوایی دنیا در سایت علی‌بابا موجود است، چه پروازهایی که مبدا یا مقصد آنها ایران است و چه پروازهای داخلی دورترین کشورهای دنیا. پروازهای ایرلاین‌هایی مثل لوفت‌هانزا، امارات، قطرایرویز، ترکیش‌ایر، ایرفرانس، کی‌ال‌ام، آئروفلوت، آلیتالیا، اوکراینی، ایرایژیا، پگاسوس و ده‌ها ایرلاین دیگر در علی بابا قابل تهیه هستند. همچنین بلیط پروازهای خارجیِ شرکت های هواپیمایی داخلی مانند ماهان، ایران‌ایر، قشم ایر، آتا و .. نیز روی سایت علی‌بابا به فروش می‌رسد.</div>
            </li>
            <li onClick={handleSlideToggle1}>
            <div id="of-ques2" > <i className="bi bi-question-circle"></i><p> چطور می توانم در مورد مقررات پرواز یا ویزا مربوط به سفرم اطمینان پیدا کنم؟</p></div>
            <div  ref={slideToggle1} >حتما قبل از انتخاب پرواز خود در مورد مقررات پرواز و قوانین مربوط به ویزا تحقیق کنید (مثلا ممکن است مقصد سفر شما نیاز به ویزا نداشته باشد ولی برای توقفی که در مسیر دارد نیاز به ویزای ترانزیت داشته باشید). برای این کار به صفحه قوانین و مقررات پرواز خارجی مراجعه کنید و در صورتی که نیاز به اطلاعات بیشتری داشتید با پشتیبانی علی بابا تماس بگیرید.میزان مجاز بار به کلاس پرواز و کلاس نرخی بلیط بستگی دارد. هنگام خرید آنلاین بلیط هواپیما می‌توانید میزان بار مجاز را در اطلاعات بلیط ببینید. طبیعی است که اگر میزان بارتان بیش از حد مجاز باشد، باید جریمه پرداخت کنید.</div>
           </li>
           <li onClick={handleSlideToggle2}>
           <div id="of-ques3" ><i className="bi bi-question-circle"></i><p>  آیا برای خرید بلیط ایرلاین‌های خارجی، نیازی به کردیت‌کارت یا روش‌های پرداخت ارزی خواهم داشت؟</p> </div>
           <div  ref={slideToggle2} >به هیچ وجه. شما هزینه‌ بلیط را به صورت ریالی و با کارت‌های شتابی که در دست دارید، تنها با داشتن رمز دوم کارت پرداخت می کنید و بلیط شما بلافاصله صادر خواهد شد و قابل استفاده خواهد بود.</div>
          </li>
          <li  onClick={handleSlideToggle3}>
           <div id="of-ques4"><i className="bi bi-question-circle"></i><p>آیا ساعت، قیمت و دیگر مشخصات پروازها روی سایت علی‌بابا قطعی است؟</p></div>
          <div  ref={slideToggle3}>بله، آنچه پس از جستجوی پرواز مورد نظر خود روی سایت علی‌بابا می‌بینید، برنامه قطعی پروازهای ایرلاین‌هاست که به ریال محاسبه شده است. اگر احیانا تغییری در ساعت یا برنامه‌ی پرواز به وجود بیاید، از طریق پشتیبانی سایت علی‌بابا به اطلاع شما خواهد رسید.</div>
         </li>
         <li onClick={handleSlideToggle4}>
         <div id="of-ques5" ><i className="bi bi-question-circle"></i><p>  آیا پروازهای چارتر خارجی هم روی سایت علی‌بابا عرضه می‌شود؟</p></div>
         <div  ref={slideToggle4} >بله. بلیط کلیه‌ی پروازهای چارتر خارجی که از سوی ایرلاین‌های داخلی یا خارجی برای فروش عرضه می‌شوند را می‌توانید روی سایت علی‌بابا ببینید.</div>
        </li>
        <li onClick={handleSlideToggle5}>
        <div id="of-ques6" ><i className="bi bi-question-circle"></i><p> چطور می توانم برای سفرم ارزان ترین بلیط ممکن را تهیه کنم؟</p></div>
        <div  ref={slideToggle5} >ارزان بودن بلیط سفر به عوامل مختلفی مرتبط است که برخی از مهم ترین آنها این موارد هستند:
        در پروازهای سیستمی معمولا هرچه به زمان پرواز نزدیک تر می شویم به دلیل پر شدن کلاس نرخی های ارزان تر، قیمت آن افزایش پیدا می کند. معمولا هرچه زودتر پروازتان را بخرید ارزان تر است.
        بلیط ایرلاین های مختلف را چک کنید. برای اغلب مسیرها ایرلاین های کم هزینه (Low Cost Airline) وجود دارند که در ازای خدمات و امکانات کمتر، پروازهای ارزان تری به شما پیشنهاد می دهند.
        در روزهای مختلف سال، فصل و هفته و حتی ساعات متفاوت، قیمت یک پرواز ممکن است تغییرات زیادی داشته باشد. با استفاده از تقویم قیمتی و فیلترهای تعبیه شده جستجو می توانید با چند روز جابجایی پروازهای به مراتب ارزان تری پیدا کنید. علاوه بر این، پروازهای توقف دار و صبح زود هم معمولا پروازهای ارزان تری هستند.
        بلیط خود را از سایت های معتبر خریداری کنید و قیمت آنها را در ازای خدمتی که می دهند با هم مقایسه کنید.
        با در نظر گرفتن این موارد می توانید هوشمندانه، بهترین و ارزان ترین پرواز را برای سفر خود تهیه کنید.</div>
       </li>
       <li onClick={handleSlideToggle6}>
       <div id="of-ques7" ><i className="bi bi-question-circle"></i><p>تقویم قیمتی (Flexible Date) چیست و چه کمکی به من می‌کند؟ </p></div>
       <div  ref={slideToggle6} >تقویم قیمتی امکان مقایسه قیمت های روزهای نزدیک به جستجو را به شما می دهد. با کلیلک روی نوار سبز رنگ تقویم قیمتی، قیمت بلیط‌های ۳ روز قبل و ۳ روز بعد از تاریخ مورد نظر شما در جدولی نمایش داده می‌شود و به این ترتیب می‌توانید با تغییر کوچکی در برنامه‌ی سفر خود، هزینه‌ی بلیط را حتی تا نصف کاهش دهید!</div>
      </li>
      <li onClick={handleSlideToggle7}>
       <div id="of-ques8" ><i className="bi bi-question-circle"></i><p> بهترین پروازها (Best Flights) چه پروازهایی هستند؟</p></div>
       <div  ref={slideToggle7} >در هر جستجویی که انجام می دهید، به صورت پیش فرض در ابتدای فهرست با پروازهایی روبرو می شوید که به عنوان بهترین پروازها به شما معرفی می شوند. در الگوریتم گزینش و نمایش این پروازها هر دو فاکتور قیمت و مدت زمان سفر در نظر گرفته شده است.</div>
      </li>
      <li onClick={handleSlideToggle8}>
       <div id="of-ques9" ><i className="bi bi-question-circle"></i><p>چگونه می توانم بلیط پروازم را کنسل کنم یا تغییر دهم و از شرایط و جریمه آن مطلع شوم؟</p></div>
       <div  ref={slideToggle8} >برای اطلاع دقیق از شرایط و جریمه کنسلی یا تغییر بلیط پرواز مورد نظرتان در هر ساعتی از شبانه روز می توانید با پشتیبانی علی‌بابا تماس بگیرید.</div>
      </li>
      <li onClick={handleSlideToggle9}>
      <div id="of-ques10" ><i className="bi bi-question-circle"></i><p> برای سوار شدن به هواپیما چه زمانی باید در فرودگاه باشم و چه مدارکی مورد نیاز است؟</p></div>
      <div  ref={slideToggle9} >برای اینکه بدون دغدغه سفر خود را آغاز کنید باید حداقل دو ساعت قبل از پرواز در فرودگاه حضور داشته باشید. برای پرواز نیازی به داشتن پرینت بلیط ندارید و تصویر و شماره بلیط به همراه مدارک شناسایی کفایت می کند.</div>
     </li>

            
        
        </ul>
       
    </div>

     );
}
 
export default OfQuesSec;