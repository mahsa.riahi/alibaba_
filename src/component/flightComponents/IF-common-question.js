import { useCallback, useRef } from 'react';
import $ from "jquery"

const IfQuesSec = () => {

    const slideToggle = useRef(null)
    const slideToggle1 = useRef(null)
    const slideToggle2 = useRef(null)
    const slideToggle3 = useRef(null)
    const slideToggle4 = useRef(null)
    const slideToggle5 = useRef(null)
    const slideToggle6 = useRef(null)
 
    
   
    const handleSlideToggle = useCallback(() =>{
        $(slideToggle.current).slideToggle("400");
    
    },[])
    const handleSlideToggle1 = useCallback(() =>{
        $(slideToggle1.current).slideToggle("400");
    
    },[])
    const handleSlideToggle2 = useCallback(() =>{
        $(slideToggle2.current).slideToggle("400");
      
    },[])
    const handleSlideToggle3 = useCallback(() =>{
        $(slideToggle3.current).slideToggle("400");
     
    },[])
    const handleSlideToggle4 = useCallback(() =>{
        $(slideToggle4.current).slideToggle("400");
       
    },[])
    const handleSlideToggle5 = useCallback(() =>{
        $(slideToggle5.current).slideToggle("400");

    },[])
    const handleSlideToggle6 = useCallback(() =>{
        $(slideToggle6.current).slideToggle("400")
    },[])


    return ( 

        <div id="ques-container">
            <h1 id="ques-title">سوالات متداول شما درباره خرید بلیط هواپیما </h1>
            <ul>
                 <li onClick={handleSlideToggle}>
                 <div id="if-ques1"  ><i className="bi bi-question-circle"></i> <p>چند روز قبل از پرواز، بلیط هواپیما را بخریم؟</p></div>
                 <div ref={slideToggle} >امکان رزرو بلیط هواپیما از ماه‌ها قبل وجود دارد. اما گاهی اوقات قیمت بلیط هواپیما در روزهای نزدیک به پرواز ارزان‌تر می‌شود. بنابراین در صورتی که شرایطتان اجازه می‌دهد، رزرو آنلاین بلیط هواپیما را به روزهای نزدیک پرواز موکول کنید. البته اگر قصد سفر در ایام پرسفر نظیر تعطیلات را دارید، باید هر چه زودتر رزرو بلیط هواپیما را انجام دهید.</div>
                </li>
                <li onClick={handleSlideToggle1}>
                <div  id="if-ques2" > <i className="bi bi-question-circle"></i><p>در هر پرواز، میزان بار مجاز چقدر است؟</p></div>
                <div  ref={slideToggle1} >میزان مجاز بار به کلاس پرواز و کلاس نرخی بلیط بستگی دارد. هنگام خرید آنلاین بلیط هواپیما می‌توانید میزان بار مجاز را در اطلاعات بلیط ببینید. طبیعی است که اگر میزان بارتان بیش از حد مجاز باشد، باید جریمه پرداخت کنید.</div>
               </li>
               <li onClick={handleSlideToggle2}>
               <div  id="if-ques3" ><i className="bi bi-question-circle"></i><p>  نرخ بلیط هواپیما برای نوزادان و کودکان زیر 12 سال چگونه است؟</p> </div>
               <div  ref={slideToggle2} >این نرخ به کلاس پرواز و کلاس نرخی بستگی دارد. اما عموما 50 تا 75 درصد قیمت بلیط بزرگسالان است. قیمت بلیط هواپیما برای نوزادان (تا 2 سال) در بیشتر موارد 10 درصد بلیط بزرگسالان است. هنگام تهیه بلیط هواپیما به این نکته توجه داشته باشید.</div>
              </li>
              <li onClick={handleSlideToggle3}>
              <div  id="if-ques4" ><i className="bi bi-question-circle"></i><p>  رزرو آنلاین بلیط هواپیما هزینه بیشتری از خرید حضوری دارد؟</p></div>
              <div  ref={slideToggle3} >خیر؛ زمانی که از یک سایت معتبر خرید بلیط هواپیما، اقدام به خرید می‌کنید، نه تنها هزینه بیشتری پرداخت نمی‌کنید، حتی ممکن است تخفیف هم بگیرید. ضمنا با خرید آنلاین بلیط هواپیما از پشتیبانی نیز برخودار هستید.</div>
             </li>
             <li onClick={handleSlideToggle4}>
             <div  id="if-ques5" ><i className="bi bi-question-circle"></i><p> آیا پس از خرید اینترنتی بلیط هواپیما امکان استرداد آن وجود دارد؟</p></div>
             <div  ref={slideToggle4} >وقتی از علی‌بابا یعنی بهترین سایت خرید بلیط هواپیما ، بلیطتان را رزرو می‌کنید، خیالتان آسوده است که امکان استرداد وجه در صورت کنسل کردن بلیط وجود دارد. میزان جریمه را هم هنگام رزرو آنلاین بلیط هواپیما در قسمت قوانین استرداد بخوانید. میزان جریمه به نوع بلیط، کلاس پروازی، کلاس نرخی و... بستگی دارد.</div>
            </li>
            <li onClick={handleSlideToggle5}>
            <div  id="if-ques6" ><i className="bi bi-question-circle"></i><p>  آیا پس از خرید بلیط هواپیما، امکان تغییر نام یا نام خانوادگی وجود دارد؟ </p></div>
            <div  ref={slideToggle5} >در پرواز داخلی یا خارجی، امکان تغییر نام و نام خانوادگی در بلیط سیستمی وجود ندارد. اما در بلیط چارتر، برخی از چارترکننده‌ها این تغییر را می‌پذیرند.</div>
           </li>
           <li onClick={handleSlideToggle6}>
           <div  id="if-ques7" ><i className="bi bi-question-circle"></i><p> هنگامی که از سایت خرید بلیط هواپیما رزرو بلیط را انجام می‌دهیم، امکان انتخاب صندلی موردنظرمان وجود دارد؟</p></div>
           <div  ref={slideToggle6} >خیر؛ هنگام رزرو بلیط هواپیما داخلی یا خارجی امکان انتخاب صندلی وجود ندارد. البته در زمان چک‌این انتخاب محدوده صندلی امکان‌پذیر است.</div>
          </li>

                
            
            </ul>
           
        </div>
     );
}
 
export default IfQuesSec;