import React , {Fragment} from 'react';
import TrainQuesSec from './trainComponents/train-common-question';
import TrainIntro from './trainComponents/train-intro';

const Train= () => {
    return ( 

        <Fragment>
             <div id="nav-train-img"  className="nav-img">
             </div>
             <TrainIntro />
             <TrainQuesSec />
        
        
        </Fragment>

     );
}
 
export default Train;