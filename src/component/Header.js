import React from 'react';


const Header = () => {
    return ( 
        
        <div id="header" className="row">

          <div className="col-lg-4 col-md-4">
                 <div className="img-service" id="img-service-1">
                        <img  src="https://cdn.alibaba.ir/dist/66efd5c0/img/desktop-intro-sprites.ddb962b.png" />
                </div>
                <h1>پوشش ۱۰۰ درصدی پروازها و قطارها و اتوبوس‌ها</h1>
                 <p>کامل‌ترین سایت خرید بلیط پروازهای داخلی، پروازهای خارجی، بلیط قطار و بلیط اتوبوس</p>
          </div>

          <div className="col-lg-4 col-md-4">
                <div className="img-service" id="img-service-2">
                        <img  src="https://cdn.alibaba.ir/dist/66efd5c0/img/desktop-intro-sprites.ddb962b.png" />
                </div>
                <h1>دسترسی آسان از طریق وبسایت، موبایل و اپلیکیشن</h1>
                 <p>ساده‌ترین و سریع‌ترین راه برای جستجو، خرید و استرداد بلیط</p>
          </div>

          <div className="col-lg-4 col-md-4">
                <div className="img-service" id="img-service-3">
                    <img  src="https://cdn.alibaba.ir/dist/66efd5c0/img/desktop-intro-sprites.ddb962b.png" />
                </div>
                <h1>قیمت رقابتی همراه با تضمین بلیط‌های چارتر</h1>
                <p>معتبرترین و به‌صرفه‌ترین سامانه فروش بلیط و چارتر با پشتیبانی ۲۴ ساعته</p>
          </div>
        
        
        </div>
    );
}
 
export default Header;